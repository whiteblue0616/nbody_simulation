#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <chrono>


#include "const.h"
#include "timer.h"

#include <hpx/hpx_init.hpp>
#include <hpx/include/iostreams.hpp>
#include <hpx/include/parallel_algorithm.hpp>
#include <hpx/include/parallel_for_loop.hpp>
#include <hpx/include/lcos.hpp>

#include <boost/iterator/counting_iterator.hpp>


struct StepResult {
        float Fx;
        float Fy;
        float Fz;
};

void randomizeBodies(float *data, int n) {
        for (int i = 0; i < n; i++) {
                data[i] = 2.0f * (rand() / (float) RAND_MAX) - 1.0f;
        }
}


void bodyForce_task(Body *p, float dt, long n) {
        StepResult *reduceVal = new StepResult;

        std::vector<hpx::future<void> > all_futures;

        for (int i = 0; i < n; i++) {
                std::vector<hpx::future<void> > req_futures;

                reduceVal->Fx = 0.0f;
                reduceVal->Fy = 0.0f;
                reduceVal->Fz = 0.0f;

                for (int j = 0; j < n; j++) {
                        hpx::future<void> newF = hpx::async([reduceVal](Body *p, int curr, int iter) {
                                float dx = p[iter].x - p[curr].x;
                                float dy = p[iter].y - p[curr].y;
                                float dz = p[iter].z - p[curr].z;
                                float distSqr = dx * dx + dy * dy + dz * dz + SOFTENING;
                                float invDist = 1.0f / sqrtf(distSqr);
                                float invDist3 = invDist * invDist * invDist;

                                reduceVal->Fx += dx * invDist3;
                                reduceVal->Fy += dy * invDist3;
                                reduceVal->Fz += dz * invDist3;
                        }, p, i, j);

                        req_futures.push_back(std::move(newF));
                }


                //        printf("%f,%f,%f\n", reduceVal->Fx, reduceVal->Fy, reduceVal->Fz);



                all_futures.push_back(
                        std::move(hpx::when_all(req_futures).then([&](hpx::future<std::vector<hpx::future<void> > >) {
                        p[i].vx += dt * reduceVal->Fx;
                        p[i].vy += dt * reduceVal->Fy;
                        p[i].vz += dt * reduceVal->Fz;
                })));
        }

        hpx::when_all(all_futures).get();

        delete reduceVal;
}

void bodyForce(Body *p, float dt, long n) {
        hpx::parallel::for_each(hpx::parallel::execution::par,
                                boost::counting_iterator<int>(0),
                                boost::counting_iterator<int>(n),
                                [&](int i) {
                float Fx = 0.0f;
                float Fy = 0.0f;
                float Fz = 0.0f;

                for (int j = 0; j < n; j++) {
                        float dx = p[j].x - p[i].x;
                        float dy = p[j].y - p[i].y;
                        float dz = p[j].z - p[i].z;
                        float distSqr = dx * dx + dy * dy + dz * dz + SOFTENING;
                        float invDist = 1.0f / sqrtf(distSqr);
                        float invDist3 = invDist * invDist * invDist;

                        Fx += dx * invDist3;
                        Fy += dy * invDist3;
                        Fz += dz * invDist3;
                }

                p[i].vx += dt * Fx;
                p[i].vy += dt * Fy;
                p[i].vz += dt * Fz;
        });

}

int hpx_main(int argc, char **argv) {
        long nBodies = 30000;
        if (argc > 1) nBodies = atoi(argv[1]);

        const float dt = 0.01f; // time step
        const int nIters = 10; // simulation iterations

        long totalTime = 0;

        for (int iter = 1; iter <= nIters; iter++) {
                long start = getTimeMillSecond();

                long bytes = nBodies * sizeof(Body);
                float *buf = (float *) malloc(bytes);
                Body *p = (Body *) buf;

                randomizeBodies(buf, 6 * nBodies); // Init pos / vel data

                bodyForce(p, dt, nBodies); // compute interbody forces

                for (long i = 0; i < nBodies; i++) { // integrate position
                        p[i].x += p[i].vx * dt;
                        p[i].y += p[i].vy * dt;
                        p[i].z += p[i].vz * dt;
                }
                long end = getTimeMillSecond();

                if (iter > 1) { // First iter is warm up
                        totalTime += (end - start);
                }

                free(buf);
        }

        std::cout << "total: " << totalTime << " ms" << std::endl;

        long avgTime = totalTime / (double) (nIters - 1);

        std::cout << "avgTime: " << avgTime << " ms" << std::endl;

        double result = (nBodies * nBodies) / avgTime;

        std::cout << nBodies << " Bodies: average " << result << " Interactions / second"
                  << std::endl;
        return hpx::finalize();
}

int main(int argc, char *argv[]) {
        return hpx::init(argc, argv);
}
