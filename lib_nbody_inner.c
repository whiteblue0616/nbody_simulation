#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>


#define SOFTENING 1e-9f

typedef struct {
        float x, y, z, vx, vy, vz;
} Body;


Body p[]={};

uint64_t func(uint64_t i, uint64_t n, float dt){

	  // printf("runnning: %d,%d\n",i,n);

          float Fx = 0.0f;
          float Fy = 0.0f;
          float Fz = 0.0f;

          for (int j = 0; j < n; j++) {
                  float dx = p[j].x - p[i].x;
                  float dy = p[j].y - p[i].y;
                  float dz = p[j].z - p[i].z;
                  float distSqr = dx * dx + dy * dy + dz * dz + SOFTENING;
                  float invDist = 1.0f / sqrtf(distSqr);
                  float invDist3 = invDist * invDist * invDist;

                  Fx += dx * invDist3;
                  Fy += dy * invDist3;
                  Fz += dz * invDist3;
          }

          p[i].vx += dt * Fx;
          p[i].vy += dt * Fy;
          p[i].vz += dt * Fz;
          //printf("VAL: %d, %d, %d\n", p[i].vx, p[i].vy, p[i].vz);
}
