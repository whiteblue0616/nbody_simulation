//
// Created by WhiteBlue on 2019-12-16.
//

#include "timer.h"

#include <sys/time.h>
#include <chrono>


long getTimeMillSecond() {
    std::chrono::steady_clock::duration d = std::chrono::steady_clock::now().time_since_epoch();
    std::chrono::milliseconds mil = std::chrono::duration_cast<std::chrono::milliseconds>(d);
    return mil.count();
}