//
// Created by WhiteBlue on 2019-12-16.
//

#ifndef NBODY_SIMULATION_CONT_H
#define NBODY_SIMULATION_CONT_H

#endif //NBODY_SIMULATION_CONT_H

#define SOFTENING 1e-9f

typedef struct {
    float x, y, z, vx, vy, vz;
} Body;
