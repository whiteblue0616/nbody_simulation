#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <chrono>

#include <ve_offload.h>

#include <hpx/hpx_init.hpp>
#include <hpx/include/iostreams.hpp>
#include <hpx/include/parallel_algorithm.hpp>
#include <hpx/include/parallel_for_loop.hpp>
#include <hpx/include/lcos.hpp>

#include <boost/iterator/counting_iterator.hpp>

#include "const.h"
#include "timer.h"

struct VEContxt {
        struct veo_thr_ctxt *ctx;
        uint64_t sym;
        uint64_t handle;
        uint64_t bufptr;
} context;

struct Profiler {
        long data_copy_time;
        long kernel_time;
        long vh_time;
        long ve_time;
} profiler;


void randomizeBodies(float *data, int n) {
        for (int i = 0; i < n; i++) {
                data[i] = 2.0f * (rand() / (float) RAND_MAX) - 1.0f;
        }
}

void load_func() {
        struct veo_proc_handle *proc = veo_proc_create(0);
        if (proc == NULL) {
                printf("veo_proc_create is failed....\n");
                exit(-1);
        }

        uint64_t handle = veo_load_library(proc, "./libnbody.so");
        printf("handle = %p\n", (void *) handle);
        if (handle <= 0) {
                printf("handle is null.....\n");
                exit(-1);
        }

        struct veo_thr_ctxt *ctx = veo_context_open(proc);
        printf("VEO context = %p\n", ctx);

        uint64_t bufptr = veo_get_sym(proc, handle, "p");

        uint64_t sym = veo_get_sym(proc, handle, "func");

        context.ctx=ctx;
        context.sym=sym;
        context.handle=handle;
        context.bufptr=bufptr;
}

void run_offload_method(Body *p, int start,int end,float dt) {
        long write_start = getTimeMillSecond();

        uint64_t retval;

        long size=sizeof(Body)*(end-start);

        Body *start_ptr=&p[start];

        uint64_t req = veo_async_write_mem(context.ctx, context.bufptr, start_ptr, size);
        // printf("veo_async_write_mem() returned %ld\n", req);

        uint64_t ret = veo_call_wait_result(context.ctx, req, &retval);
        // printf("write memory 0x%lx: %d, %lu\n", req, ret, retval);

        long write_end = getTimeMillSecond();

        profiler.data_copy_time += (write_end - write_start);

        struct veo_args *argp = veo_args_alloc();
        veo_args_set_i32(argp, 0, start);
        veo_args_set_i32(argp, 1, end);
        veo_args_set_float(argp, 2, dt);

        uint64_t id = veo_call_async(context.ctx, context.sym, argp);
        // printf("veo_call_async() returned %ld\n", id);

        ret = veo_call_wait_result(context.ctx, id, &retval);
        // printf("call method 0x%lx: %d, %lu\n", req, ret, retval);

        long read_start = getTimeMillSecond();

        req = veo_async_read_mem(context.ctx, start_ptr, context.bufptr, size);
        // printf("veo_async_read_mem() returned %ld\n", req);
        ret = veo_call_wait_result(context.ctx, req, &retval);
        // printf("read memory 0x%lx: %d, %lu\n", req, ret, retval);

        long read_end = getTimeMillSecond();

        profiler.data_copy_time += (read_end - read_start);

        veo_args_free(argp);

        long kernel_end = getTimeMillSecond();

        profiler.kernel_time += (kernel_end - write_start);
}

void do_close(){
        int close_status = veo_context_close(context.ctx);
        printf("close status = %d\n", close_status);
}

void bodyForce(Body *p, float dt, long n) {
        // int mid=n/4;

        hpx::future<void> vh_future=hpx::async([&](){
                long vh_start = getTimeMillSecond();
                hpx::parallel::for_each(hpx::parallel::execution::par,
                                        boost::counting_iterator<int>(0),
                                        boost::counting_iterator<int>(n/2),
                                        [&](int i) {
                        float Fx = 0.0f;
                        float Fy = 0.0f;
                        float Fz = 0.0f;

                        for (int j = 0; j < n; j++) {
                                float dx = p[j].x - p[i].x;
                                float dy = p[j].y - p[i].y;
                                float dz = p[j].z - p[i].z;
                                float distSqr = dx * dx + dy * dy + dz * dz + SOFTENING;
                                float invDist = 1.0f / sqrtf(distSqr);
                                float invDist3 = invDist * invDist * invDist;

                                Fx += dx * invDist3;
                                Fy += dy * invDist3;
                                Fz += dz * invDist3;
                        }

                        p[i].vx += dt * Fx;
                        p[i].vy += dt * Fy;
                        p[i].vz += dt * Fz;
                });
                long vh_end = getTimeMillSecond();
                profiler.vh_time += (vh_end - vh_start);
        });

        hpx::future<void> ve_future=hpx::async([&](){
                long ve_start = getTimeMillSecond();
                run_offload_method(p,n/2+1,n,dt);
                long ve_end = getTimeMillSecond();
                profiler.ve_time += (ve_end - ve_start);
        });

        hpx::when_all(vh_future,ve_future).get();
}

int hpx_main(int argc, char **argv) {
        long nBodies = 30000;
        if (argc > 1) nBodies = atoi(argv[1]);

        const float dt = 0.01f; // time step
        const int nIters = 10; // simulation iterations

        long totalTime = 0;

        for (int iter = 1; iter <= nIters; iter++) {
                long start = getTimeMillSecond();

                long bytes = nBodies * sizeof(Body);
                float *buf = (float *) malloc(bytes);
                Body *p = (Body *) buf;

                randomizeBodies(buf, 6 * nBodies); // Init pos / vel data

                bodyForce(p, dt, nBodies); // compute interbody forces

                for (long i = 0; i < nBodies; i++) { // integrate position
                        p[i].x += p[i].vx * dt;
                        p[i].y += p[i].vy * dt;
                        p[i].z += p[i].vz * dt;
                }
                long end = getTimeMillSecond();

                if (iter > 1) { // First iter is warm up
                        totalTime += (end - start);
                }

                free(buf);
        }

        std::cout << "total: " << totalTime << " ms" << std::endl;

        double avgTime = totalTime / (double) (nIters - 1);

        std::cout << "avgTime: " << avgTime << " ms" << std::endl;

        double result = (nBodies * nBodies) / avgTime;

        std::cout << nBodies << " Bodies: average " << result << " Interactions / second" << std::endl;

        std::cout << "data load time: "<< profiler.data_copy_time << " ms, kernel time: " << profiler.kernel_time<< " ms"<< std::endl;

        std::cout << "VH time: "<< profiler.vh_time << " ms, VE time: " << profiler.ve_time<< " ms"<< std::endl;

        do_close();

        return hpx::finalize();
}

int main(int argc, char *argv[]) {
        load_func();
        return hpx::init(argc, argv);
}
